/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 19:47:04 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 21:38:41 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strequ() function compare const char *s1 and const char *s2 and
** return if they are strictly equal or not
**
** RETURN VALUES:
**  SUCCESS: int 1 if const char *s1 and const char *s2 is equal
**  FAILURE: int zero
*/

int	ft_strequ(const char *s1, const char *s2)
{
	if (!s1 || !s2)
		return (-1);
	if (!*s1 && !*s2)
		return (1);
	while (*s1 == *s2)
	{
		s1++;
		s2++;
		if (!*s1 && !*s2)
			return (1);
	}
	if (*s1 != *s2)
		return (0);
	return (1);
}
