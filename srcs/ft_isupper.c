/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:48:47 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:12:30 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_isupper() function tests for any upper-case letter.  The value of the
** argument must be representable as an unsigned char or the value of EOF.
**
** RETURN VALUES:
**  SUCCESS: int 1 if the test succeed
**  FAILURE: int zero
*/

int	ft_isupper(int c)
{
	return ((c >= 65 && c <= 90));
}
