/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:50:28 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:12:04 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_isdigit() function tests for a decimal digit character.
** The value of the argument must be representable as an unsigned char or the
** value of EOF.
**
** RETURN VALUES:
**  SUCCESS: int 1 if the test succeed
**  FAILURE: int zero
*/

int	ft_isdigit(int c)
{
	return (c >= '0' && c <= '9');
}
