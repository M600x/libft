/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:25:41 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:41:27 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_toupper() function check if int c (as char) is a lower letter
** and return an int value of it as a capital letter
**
** RETURN VALUES:
**  SUCCESS: int c of the same letter int c but as lower
**  FAILURE: int c
*/

int	ft_toupper(int c)
{
	if (c >= 97 && c <= 122)
		c = c - 32;
	return (c);
}
