/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sstradd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:20:58 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:53:58 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_sstradd() function allocate (with malloc(3)) a new **str and add
** the char *add to it. It will delete char **s and set the pointer to NULL.
**
** RETURN VALUES:
**  SUCCESS: char ** of char **s with char *add
**  FAILURE: NULL
*/

char	**ft_sstradd(char **s, char *add)
{
	char	**rt;
	int		i;

	i = 0;
	rt = ft_sstrnew(ft_sstrlen(s) + 1);
	while (s[i])
	{
		rt[i] = ft_strdup(s[i]);
		i++;
	}
	rt[i] = ft_strdup(add);
	ft_sstrdel(s);
	s = rt;
	return (rt);
}
