/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:48:47 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:35:30 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strtoupper() function will change all letter to capital
**
** RETURN VALUES:
**  SUCCESS: char *str with all letter changed to capital
**  FAILURE: char *str
*/

char	*ft_strtoupper(char *str)
{
	while (*str)
	{
		if (ft_isupper(*str))
			*str = *str - 32;
		str++;
	}
	return (str);
}
