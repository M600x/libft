/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 13:15:53 by alao              #+#    #+#             */
/*   Updated: 2016/12/02 13:55:21 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The get_next_line() function will read the file descriptor pointed by fd
** and read until it find a return '\n' or an EOF and copy the readed string
** in char **line allocated with malloc(3). All string are without the return
** '\n' and closed with an EOF.
** If the file descriptor still contain data in it, the function will return
** int 1 to mark it so the function can be recalled at the exact same position.
** If the file descriptor is over and an EOF is detected, the function will
** return an int 0 to indicate that the reading ended. File descriptor is not
** closed by the function. It must be done outside the function.
** If any error occurred during the function, it will return int -1.
** A define MFD is set as the max number of file descriptor the function
** can handle.
**
** RETURN VALUES:
**  SUCCESS: int  1 if the file descriptor still contain data.
**           int  0 if the file descriptor is finished
**  FAILURE: int -1 if an error occurred
*/

static int		ft_output(t_rd *r, char **line, t_rd **pr)
{
	if (r->rs < 0 || r->fs <= 0)
		*pr = NULL;
	if (r->rs < 0)
		return (-1);
	if (r->fs <= 0)
	{
		r->now ? ft_memdel((void *)&r->now) : (0);
		free(r);
		return (0);
	}
	*line = ft_strncpy(ft_strnew(r->ri), r->now, r->ri);
	r->fs += (r->now - r->prev) - (*(r->prev) == '\n');
	ft_strncpy(r->now, (r->prev + 1), r->fs);
	r->prev = r->now;
	return (1);
}

static int		ft_detector(t_rd *r)
{
	while (*(r->prev) != '\n' && (r->fs > r->prev - r->now))
		r->prev++;
	r->ri = r->prev - r->now;
	if (*(r->prev) == '\n')
		return (r->fs > (r->prev - r->now));
	return (0);
}

static t_rd		*ft_resizer(t_rd *r)
{
	char		*n_now;

	if (!(n_now = ft_strcpy(ft_strnew(r->len * 2), r->now)))
		return (NULL);
	r->prev = n_now + (r->prev - r->now);
	ft_memdel((void *)&r->now);
	r->now = n_now;
	r->len = r->len * 2;
	return (r);
}

int				get_next_line(int const fd, char **line)
{
	static t_rd	*r[MFD];

	if (!line || fd < 0 || fd > MFD || BUFF_SIZE < 1)
		return (-1);
	if (!r[fd])
	{
		if (!(r[fd] = (t_rd *)malloc(sizeof(t_rd))))
			return (-1);
		r[fd]->now = ft_strnew(BUFF_SIZE);
		r[fd]->prev = r[fd]->now;
		r[fd]->len = BUFF_SIZE;
		r[fd]->fs = 0;
		r[fd]->rs = 1;
	}
	while (r[fd]->rs > 0 && !(ft_detector(r[fd])))
	{
		while (r[fd]->fs + BUFF_SIZE > r[fd]->len)
			if (!(ft_resizer(r[fd])))
				return (-1);
		r[fd]->rs = read(fd, r[fd]->now + r[fd]->fs, BUFF_SIZE);
		r[fd]->fs += r[fd]->rs;
	}
	return (ft_output(r[fd], line, &r[fd]));
}
