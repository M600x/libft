/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strxcchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:08:16 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:56:42 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strxcchr() function search the int c (converted to char) in the
** const char *s and return the number of time it as been found.
**
** RETURN VALUES:
**  SUCCESS: int of number of time where int c is found in const char *s
**  FAILURE: int zero
*/

int	ft_strxcchr(const char *s, int c)
{
	int i;
	int rt;

	i = 0;
	rt = 0;
	while (s[i])
	{
		if (s[i] == (char)c)
			rt++;
		i++;
	}
	return (rt);
}
