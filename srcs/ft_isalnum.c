/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:47:43 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:11:52 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_isalnum() function tests for any character for which isalpha() or
** isdigit() is true.  The value of the argument must be representable as
** an unsigned char or the value of EOF.
**
** RETURN VALUES:
**  SUCCESS: int pos/neg if the test succeed
**  FAILURE: int zero
*/

int	ft_isalnum(int c)
{
	return (ft_isalpha(c) || ft_isdigit(c));
}
