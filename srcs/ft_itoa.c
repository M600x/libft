/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:52:19 by alao              #+#    #+#             */
/*   Updated: 2016/11/08 15:54:05 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_itoa() allocate (with malloc(3)) a string and return the converted
** value of the int n (propelly terminated with \0) passed in parameter.
** Negative value are supported. If the allocation fail, the function returns
** NULL.
**
** RETURN VALUES:
**  SUCCESS: char * of the converted int n
**  FAILURE: NULL
*/

static char	ft_isneg(int *n, char c)
{
	c = '-';
	(*n == -2147483648) ? (*n = 2147483647) : (*n *= -1);
	return (c);
}

char		*ft_itoa(int n)
{
	char	*str;
	int		i;
	int		isave;
	int		nsave;

	i = ft_length_int(n);
	(n < 0) ? i++ : (0);
	isave = i;
	if (!(str = ft_strnew(i)))
		return (NULL);
	nsave = n;
	(n < 0) ? i-- : (0);
	(n < 0) ? str[0] = ft_isneg(&n, str[0]) : (0);
	(n < 0) ? (0) : (str[i--] = '\0');
	(n == 0) ? str[0] = '0' : (0);
	while (n != 0)
	{
		str[i] = '0' + n % 10;
		n /= 10;
		i--;
	}
	(nsave == -2147483648) ? (str[isave] += 1) : (str[0] = str[0]);
	str[isave] = '\0';
	return (str);
}
