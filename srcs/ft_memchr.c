/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:56:52 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:52:45 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memchr() function locates the first occurrence of c (converted to an
** unsigned char) in string s.
**
** RETURN VALUES:
**  SUCCESS: void * to the byte located
**  FAILURE: NULL
*/

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*v;

	v = (unsigned char *)s;
	while (n)
	{
		if (*v == (unsigned char)c)
			return ((void *)v);
		n--;
		v++;
	}
	return (NULL);
}
