/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_long.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:52:19 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:52:10 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_itoa_long() allocate (with malloc(3)) a string and return the
** converted value of the long long nb (propelly terminated with \0) passed in
** parameter. Negative value are supported. If the allocation fail,
** the function returns NULL.
**
** RETURN VALUES:
**  SUCCESS: char * of the converted nb
**  FAILURE: NULL
*/

static char	ft_longisneg(long long *n, char c)
{
	c = '-';
	(*n == -9223372036854775807) ? (*n = 9223372036854775807) : (*n *= -1);
	return (c);
}

char		*ft_itoa_long(long long nb)
{
	char		*str;
	int			i;
	int			isave;
	long long	nsave;

	i = ft_length_long(nb);
	isave = i;
	nsave = nb;
	if (!(str = ft_strnew(i)))
		return (NULL);
	if (nb < 0)
		str[0] = ft_longisneg(&nb, str[0]);
	else
		str[i--] = '\0';
	(nb == 0) ? (str[0] = '0') : (0);
	while (nb != 0)
	{
		str[i] = '0' + nb % 10;
		nb /= 10;
		i--;
	}
	(nsave == -9223372036854775807) ? (str[isave] += 1) : (str[0] = str[0]);
	str[isave + 1] = '\0';
	return (str);
}
