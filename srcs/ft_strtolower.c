/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtolower.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:48:47 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:35:09 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strtolower() function will change all letter to lower
**
** RETURN VALUES:
**  SUCCESS: char *str with all letter changed to lower
**  FAILURE: char *str
*/

char	*ft_strtolower(char *str)
{
	while (*str)
	{
		if (ft_islower(*str))
			*str = *str + 32;
		str++;
	}
	return (str);
}
