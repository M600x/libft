/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:38:15 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:49:45 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strlcat() function
**
** RETURN VALUES:
**  SUCCESS:
**  FAILURE:
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	d_length;
	size_t	t_length;

	d_length = ft_strlen(dst);
	t_length = d_length + ft_strlen(src);
	if (!(dst = (char *)ft_memchr(dst, '\0', size)))
		return (size + ft_strlen(src));
	while (*src && d_length < size - 1)
	{
		*dst++ = *src++;
		d_length++;
	}
	*dst = '\0';
	return (t_length);
}
