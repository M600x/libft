/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rgb_extractor.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 13:56:38 by alao              #+#    #+#             */
/*   Updated: 2016/12/02 13:56:49 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_rgb_extractor(int color, char c)
{
	if (c == 'r')
		return ((color >> 16) & 0xFF);
	if (c == 'g')
		return ((color >> 8) & 0xFF);
	if (c == 'b')
		return ((color) & 0xFF);
	return (-1);
}
