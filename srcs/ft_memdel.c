/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:59:13 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:30:51 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memdel() function take a pointer to a memory zone that will be free
** (with free(3)) then the pointer is set to NULL.
**
** RETURN VALUES:
**  SUCCESS: NULL
**  FAILURE: NULL
*/

void	*ft_memdel(void **ap)
{
	if (ap)
	{
		free(*ap);
		*ap = NULL;
	}
	return (NULL);
}
