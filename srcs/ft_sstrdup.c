/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sstrdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 01:18:44 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:54:07 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_sstrdup() will create a new **str with malloc (3) and duplicate
** the char **src to it. Return value is the duplicate char **src.
**
** RETURN VALUES:
**  SUCCESS: char ** duplicated from char **src
**  FAILURE: NULL
*/

char			**ft_sstrdup(char **src)
{
	char	**dest;
	int		i;

	if (!(src))
		return (NULL);
	dest = NULL;
	i = 0;
	dest = ft_sstrnew(ft_sstrlen(src));
	while (src[i])
	{
		dest[i] = ft_strdup(src[i]);
		i++;
	}
	dest[i] = NULL;
	return (dest);
}
