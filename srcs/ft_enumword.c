/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_enumword.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 20:20:16 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:07:26 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_enumword() count the number of word in string s separated with the
** char c (e.g space, point, etc).
**
** RETURN VALUES:
**  SUCCESS: size_t number of word cutted by c
**  FAILURE: size_t zero
*/

size_t	ft_enumword(const char *s, char c)
{
	size_t w;

	w = 0;
	while (*s)
	{
		while (*s && *s == c)
			s++;
		if (*s)
			w++;
		while (*s && *s != c)
			s++;
	}
	return (w);
}
