/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 00:00:00 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:52:38 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memccpy() function copies bytes from string src to string dst.  If the
** character c (as converted to an unsigned char) occurs in the string src,
** the copy stops and a pointer to the byte after the copy of c in the string
** dst is returned.  Otherwise, n bytes are copied, and a NULL pointer is
** returned.
** The source and destination strings should not overlap, as the behavior is
** undefined.
**
** RETURN VALUES:
**  SUCCESS: void * to the char c found
**  FAILURE: NULL
*/

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char	*v_dst;
	unsigned char	*v_src;
	int				i;

	i = 0;
	v_dst = (unsigned char *)dst;
	v_src = (unsigned char *)src;
	while (n)
	{
		v_dst[i] = v_src[i];
		if (v_src[i] == (unsigned char)c)
			return (&((char *)v_dst)[i + 1]);
		n--;
		i++;
	}
	return (NULL);
}
