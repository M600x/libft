/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapital.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:48:47 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:54:33 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strcapital() function will change all lower letter to capital
**
** RETURN VALUES:
**  SUCCESS: char *str with all the lower letter changed to capital
**  FAILURE: char *str
*/

char	*ft_strcapital(char *str)
{
	int i;

	i = 0;
	ft_strtolower(str);
	while (str[i] != '\0')
	{
		while (ft_isalpha(str[i]) == 0 && str[i] != '\0')
			i++;
		if (str[i - 1] >= 48 && str[i - 1] <= 57 && str[i] != '\0')
			i++;
		else if (str[i] != '\0')
			str[i] = str[i] - 32;
		while (ft_isalpha(str[i]) == 1)
			i++;
	}
	return (str);
}
