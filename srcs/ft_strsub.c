/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 19:57:30 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:34:21 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strsub() function allocate with malloc(3) a char *str and copy
** char const *s into it for unsigned int start for size_t len
**
** RETURN VALUES:
**  SUCCESS: char *str of char const *s starting at unsigned int star
**  FAILURE: NULL
*/

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;
	int		i;

	i = 0;
	if (!(str = ft_strnew(len + 1)))
		return (NULL);
	while (len-- > 0 && s[start])
		str[i++] = s[start++];
	return (str);
}
