/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_factor.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:06:57 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:07:19 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_factor calculate the factorial number of the int nb and return it.
**
** RETURN VALUES:
**  SUCCESS: int factorial of nb
**  FAILURE: int zero
*/

int	ft_factor(int nb)
{
	if (nb == 0 || nb == 1)
		return (1);
	return (nb * ft_factor(nb - 1));
}
