/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:00:12 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:53:33 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memset() function writes len bytes of value c (converted to an
** unsigned char) to the string b.
**
** RETURN VALUES:
**  SUCCESS: void * b
**  FAILURE: void * b
*/

void*ft_memset(void *b, int c, size_t n)
{
	unsigned char *v;

	v = (unsigned char *)b;
	while (n)
	{
		*v = (unsigned char)c;
		v++;
		n--;
	}
	return (b);
}
