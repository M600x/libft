/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_enumletter.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 20:20:16 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:07:31 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_enumletter() count the number of char in string s without the
** char c as cutting char (e.g space, point, etc).
**
** RETURN VALUES:
**  SUCCESS: size_t of the number of char minus the char c
**  FAILURE: size_t zero
*/

size_t	ft_enumletter(const char *s, char c)
{
	size_t l;

	l = 0;
	while (*s)
	{
		while (*s && *s != c)
			s++;
		if (*s)
			l++;
		while (*s && *s == c)
			s++;
	}
	return (l);
}
