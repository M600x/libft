/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:45:38 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:07:38 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_atoi() function convert the initial portion of the string pointed
** by the const char *str to an int representation.
**
** RETURN VALUES:
**  SUCCESS: int value of *str
**  FAILURE: int zero
*/

int		ft_atoi(const char *str)
{
	int result;
	int pos;

	result = 0;
	pos = 1;
	while ((*str > 8 && *str < 14) || (*str == ' '))
		str++;
	if ((*str == '-') || (*str == '+'))
		if ((pos = ((*str == '-') ? -pos : pos)))
			str++;
	while (*str == '0')
		str++;
	while (*str >= '0' && *str <= '9')
	{
		result = (result * 10) + ((char)*str - 48);
		str++;
	}
	return (result * pos);
}
