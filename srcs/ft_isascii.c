/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:49:37 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:12:00 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_isascii() function tests for an ASCII character, which is any
** character between 0 and octal 0177 inclusive.
**
** RETURN VALUES:
**  SUCCESS: int 1 if the test succeed
**  FAILURE: int zero
*/

int	ft_isascii(int c)
{
	return (c >= 0 && c <= 127);
}
