/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:13:03 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 21:34:00 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strdel() function free and set to NULL the char **as
**
** RETURN VALUES:
**  SUCCESS: NULL
**  FAILURE: NULL
*/

void	*ft_strdel(char **as)
{
	if (as)
	{
		free(*as);
		*as = NULL;
	}
	return (NULL);
}
