/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:23:34 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:55:58 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strstr() function compare the const char *s1 and const char *s2.
** If a difference is found, the function return a char pointer in char *s1
** where it occurred.
**
** RETURN VALUES:
**  SUCCESS: char * in const char *s1
**  FAILURE: NULL
*/

char	*ft_strstr(const char *s1, const char *s2)
{
	if (!*s2)
		return ((char *)s1);
	while (*s1)
	{
		if (ft_strncmp(s1, s2, ft_strlen(s2)) == 0)
			return ((char *)s1);
		s1++;
	}
	return (NULL);
}
