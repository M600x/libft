/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 00:00:00 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:05:18 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_bzero() function writes n zeroed bytes to the string s.
** If n is zero, bzero() does nothing.
**
** RETURN VALUES:
**  SUCCESS: Nothing
**  FAILURE: Nothing
*/

void	ft_bzero(void *s, size_t n)
{
	ft_memset(s, '\0', n);
}
