/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 20:20:16 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:55:53 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strsplit() function allocate with malloc(3) a char ** containing the
** char const *s cutted with char c
**
** RETURN VALUES:
**  SUCCESS: char ** containing char const *s cutted with char c
**  FAILURE: NULL
*/

static	int		ft_cutline(char const *str, char c)
{
	size_t	i;

	i = 0;
	while (str[i] == c && str[i] != '\0')
		i++;
	while (str[i] != c && str[i] != '\0')
		i++;
	return (i);
}

static	char	*ft_strclear(char *str, char c)
{
	char *str2;
	char *rt;

	if (*str == '\0')
		return (str);
	while (*str == c && *str != '\0')
		str++;
	str2 = ft_strdup(str);
	rt = ft_strtrim(str2);
	ft_memdel((void *)&str2);
	return (rt);
}

static int		counter(char const *s, char c)
{
	int rt;
	int i;

	rt = 1;
	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			rt++;
		i++;
	}
	return (rt);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**tabstr;
	char	*tmp;
	size_t	y;

	y = 0;
	if (!s || !c)
		return (NULL);
	if (!(tabstr = (char **)malloc(sizeof(char *) * (counter(s, c) + 1))))
		return (NULL);
	while (*s)
	{
		tmp = ft_strncpy(ft_strnew(ft_cutline(s, c) + 1), s, ft_cutline(s, c));
		tabstr[y] = ft_strclear(tmp, c);
		(tmp[0] == '\0') ? (tabstr[y++] = NULL) : (0);
		ft_memdel((void *)&tmp);
		s += ft_cutline(s, c);
		y++;
	}
	tabstr[y] = NULL;
	return (tabstr);
}
