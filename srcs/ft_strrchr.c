/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:22:33 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:55:48 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strrchr() function search the int c (converted to char) in the
** const char *s without changing the initial pointer of const char *s
**
** RETURN VALUES:
**  SUCCESS: char * to the position of int c
**  FAILURE: NULL
*/

char	*ft_strrchr(const char *s, int c)
{
	size_t len;

	len = ft_strlen(s);
	while (s[len] != (char)c && len <= ft_strlen(s))
		len--;
	if (s[len] == (char)c)
		return (&((char *)s)[len]);
	return (NULL);
}
