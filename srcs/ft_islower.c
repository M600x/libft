/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_islower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:48:47 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:12:08 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_islower() function tests for any lower-case letters.  The value of the
** argument must be representable as an unsigned char or the value of EOF.
**
** RETURN VALUES:
**  SUCCESS: int 1 if the test succeed
**  FAILURE: int 0
*/

int	ft_islower(int c)
{
	return ((c >= 97 && c <= 122));
}
