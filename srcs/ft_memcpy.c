/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 00:00:00 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:53:04 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memcpy() function copies n bytes from memory area src to memory
** area dst.
** If dst and src overlap, behavior is undefined.  Applications in which dst and
** src might overlap should use ft_memmove(3) instead.
**
** RETURN VALUES:
**  SUCCESS: void * dst
**  FAILURE: void * dst
*/

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char	*v_dst;
	unsigned char	*v_src;
	int				i;

	i = 0;
	v_dst = (unsigned char *)dst;
	v_src = (unsigned char *)src;
	while (n)
	{
		v_dst[i] = v_src[i];
		n--;
		i++;
	}
	return (dst);
}
