/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:11:47 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 21:32:05 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strcmp() function compare the const char *s1 and const char *s2. If
** a difference is found, the function return the differencial value of the
** two char converted as unsigned char.
**
** RETURN VALUES:
**  SUCCESS: int value of the difference on const char *s1 and const char *s2
**  FAILURE: int zero
*/

int	ft_strcmp(const char *s1, const char *s2)
{
	if (!*s1 && !*s2)
		return (0);
	while (*s1 == *s2)
	{
		s1++;
		s2++;
		if (!*s1 && !*s2)
			return (0);
	}
	if (*s1 != *s2)
		return ((unsigned char)*s1 - (unsigned char)*s2);
	return (0);
}
