/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:13:36 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 21:35:30 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strdup() function return a char *dest allocated with malloc(3)
** containing a copy of const char *s
**
** RETURN VALUES:
**  SUCCESS: char *dest copy of const char *s
**  FAILURE: NULL
*/

char	*ft_strdup(const char *s)
{
	int		i;
	char	*dest;

	i = 0;
	if (!(dest = ft_strnew(ft_strlen(s))))
		return (NULL);
	while (s[i])
	{
		dest[i] = s[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
