/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:55:11 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:52:33 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memalloc() allocate (with malloc(3)) using the params size. The
** new memory space is initialized with '\0'. If the allocation fail, the
** function return NULL.
**
** RETURN VALUES:
**  SUCCESS: void * of size_t size
**  FAILURE: NULL
*/

void	*ft_memalloc(size_t size)
{
	void	*mem;

	if (!(mem = (void *)malloc(size)))
		return (NULL);
	ft_memset(mem, '\0', size);
	return (mem);
}
