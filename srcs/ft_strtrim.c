/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 20:10:29 by alao              #+#    #+#             */
/*   Updated: 2016/10/22 16:33:39 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strtrim() function will allocate a char *s and copy char const *s
** without the leading and ending blank elements including:
** space, tab or newline return
**
** RETURN VALUES:
**  SUCCESS: char * cleaned of whitespaces
**  FAILURE: char *
*/

char	*ft_strtrim(char const *s)
{
	int		i;
	int		len;
	char	*str;

	i = 0;
	str = NULL;
	if (ft_strlen(s) == 0)
		return (ft_strnew(0));
	len = ft_strlen(s);
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		i++;
	if (i == (int)ft_strlen(s))
		return (ft_strnew(0));
	while (len > 1 && (s[len - 1] == ' ' || s[len - 1] == '\t'
			|| s[len - 1] == '\n'))
		len--;
	str = ft_strsub(s, i, len - i);
	return (str);
}
