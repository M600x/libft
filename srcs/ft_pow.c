/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:06:57 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:32:32 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_pow() function calculate the power of the int nb using int power.
**
** RETURN VALUES:
**  SUCCESS: int of power nb
**  FAILURE: int 1
*/

int	ft_pow(int nb, int power)
{
	int result;
	int pos;

	result = 1;
	pos = 1;
	if (power == 0 || power == 1 || nb == 1)
		return (1);
	pos = power < 0 ? -pos : pos;
	while (power)
	{
		result = result * nb;
		power--;
	}
	return (result * pos);
}
