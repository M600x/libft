/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:08:16 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:54:42 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strcchr() function search the int c (converted to char) in the
** const char *s.
**
** RETURN VALUES:
**  SUCCESS: int position of int c in char const *s
**  FAILURE: int zero
*/

int	ft_strcchr(const char *s, int c)
{
	int i;

	i = 0;
	while (s[i] != (char)c && s[i])
		i++;
	if (s[i] == (char)c)
		return (i);
	return (0);
}
