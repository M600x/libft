/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:19:04 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 21:53:55 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strncmp() function compare the const char *s1 and const char *s2 for
** size_t n time. If a difference is found, the function return the differencial
** value of the two char converted as unsigned char.
**
** RETURN VALUES:
**  SUCCESS: int value of the difference on const char *s1 and const char *s2
**  FAILURE: int zero
*/

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	if (!*s1 && !*s2 && n > 0)
		return (0);
	while (*s1 == *s2 && n > 0)
	{
		s1++;
		s2++;
		if (!*s1 && !*s2)
			return (0);
		n--;
	}
	if (*s1 != *s2 && n > 0)
		return ((unsigned char)*s1 - (unsigned char)*s2);
	return (0);
}
