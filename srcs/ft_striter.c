/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:20:34 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 21:40:12 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_striter() function pass the char contained in char *s to a function
** pointed by void *f
**
** RETURN VALUES:
**  SUCCESS: Nothing
**  FAILURE: Nothing
*/

void	ft_striter(char *s, void (*f)(char *))
{
	int i;

	i = 0;
	if (!s || !*f)
		return ;
	while (s[i])
	{
		f(&s[i]);
		i++;
	}
}
