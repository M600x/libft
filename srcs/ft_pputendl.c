/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pputendl.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 01:46:21 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:32:47 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_pputendl() function write the char const *s and char const *p on
** stdout and put a return afterward.
**
** RETURN VALUES:
**  SUCCESS: Nothing
**  FAILURE: Nothing
*/

void	ft_pputendl(char const *s, char const *p)
{
	write(1, s, ft_strlen(s));
	write(1, p, ft_strlen(p));
	write(1, "\n", 1);
}
