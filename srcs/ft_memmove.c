/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 00:00:00 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:53:12 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memmove() function copies len bytes from string src to string dst.
** The two strings may overlap; the copy is always done in a non-destructive
** manner.
**
** RETURN VALUES:
**  SUCCESS: void * dst
**  FAILURE: void * dst
*/

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t	i;
	char	*tmp;

	i = 0;
	tmp = ft_memcpy(ft_strnew(len), src, len);
	while (i < len)
	{
		((char *)dst)[i] = ((char *)tmp)[i];
		i++;
	}
	return (dst);
}
