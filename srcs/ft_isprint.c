/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:51:16 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:12:11 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_isprint() function tests for any printing character, including
** space (` '). The value of the argument must be representable as an unsigned
** char or the value of EOF.
**
** RETURN VALUES:
**  SUCCESS: int 1 if the test succeed
**  FAILURE: int 0
*/

int	ft_isprint(int c)
{
	if (c > 31 && c < 127)
		return (1);
	else
		return (0);
}
