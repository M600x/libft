/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:57:42 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:52:55 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_memcmp() function compares byte string s1 against byte string s2.
** Both strings are assumed to be n bytes long.
** The ft_memcmp() function returns zero if the two strings are identical,
** otherwise returns the difference between the first two differing bytes
** (treated as unsigned char values, so that `\200' is greater than `\0').
** Zero-length strings are always identical.
**
** RETURN VALUES:
**  SUCCESS: int difference between the two void pointer s1 and s2
**  FAILURE: int zero
*/

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*v_s1;
	unsigned char	*v_s2;
	size_t			i;

	i = 0;
	v_s1 = (unsigned char *)s1;
	v_s2 = (unsigned char *)s2;
	while (n > i)
	{
		if (v_s1[i] == v_s2[i])
			i++;
		else
			return (v_s1[i] - v_s2[i]);
	}
	return (0);
}
