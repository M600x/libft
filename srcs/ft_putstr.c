/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:06:57 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 20:34:14 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_putstr() function write the char const *s.
**
** RETURN VALUES:
**  SUCCESS: Nothing
**  FAILURE: Nothing
*/

void	ft_putstr(char const *s)
{
	write(1, s, ft_strlen(s));
}
