/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcchr_rev.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:08:16 by alao              #+#    #+#             */
/*   Updated: 2016/10/21 23:26:51 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strcchr() function search the int c (converted to char) in the
** const char *s.
**
** RETURN VALUES:
**  SUCCESS: int position of int c in char const *s
**  FAILURE: int zero
*/

int	ft_strcchr_rev(const char *s, int c)
{
	int i;

	i = ft_strlen(s) - 1;
	while (i >= 0 && s[i] && s[i] != (char)c)
		i--;
	if (i >= 0 && s[i] == (char)c)
		return (i);
	return (0);
}
