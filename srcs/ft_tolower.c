/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:25:01 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:41:01 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_tolower() function check if int c (as char) is a capital letter
** and return an int value of it as a lower letter
**
** RETURN VALUES:
**  SUCCESS: int c of the same letter int c but as capital
**  FAILURE: int c
*/

int	ft_tolower(int c)
{
	if (c >= 65 && c <= 90)
		c = c + 32;
	return (c);
}
