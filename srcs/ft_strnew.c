/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:20:58 by alao              #+#    #+#             */
/*   Updated: 2016/10/09 23:55:37 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_strnew() function allocate with malloc(3) a char *set of size_t size
** initialized to zero.
**
** RETURN VALUES:
**  SUCCESS: char * of size_t size
**  FAILURE: NULL
*/

void	*ft_strnew(size_t size)
{
	char	*set;

	if (!(set = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	ft_memset(set, '\0', size + 1);
	return (set);
}
