/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:06:57 by alao              #+#    #+#             */
/*   Updated: 2016/10/22 17:49:48 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** The ft_sqrt() function calculate the square number of int nb,
**
** RETURN VALUES:
**  SUCCESS: int of square nb
**  FAILURE: int zero
*/

int	ft_sqrt(int nb)
{
	int i;

	i = 1;
	while (nb - (i * i) > 0)
		i++;
	if (i * i == nb)
		return (i);
	return (0);
}
