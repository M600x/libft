# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alao <alao@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/27 15:45:38 by alao              #+#    #+#              #
#    Updated: 2016/12/02 13:57:25 by alao             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all, clean, fclean, re

NAME = libft.a

NOC=\033[0m
OKC=\033[94;1m
ERC=\033[31m
WAC=\033[33m

CC = gcc
CC_FLAGS = -Wall -Werror -Wextra

SRC_PATH = ./srcs/
OBJ_PATH = ./obj/
INC_PATH = ./includes/

SRC = $(addprefix $(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))

OBJ_NAME = $(SRC_NAME:.c=.o)

INC_NAME += libft.h

SRC_NAME +=	ft_memchr.c		ft_memcmp.c		ft_memcpy.c		ft_memccpy.c	\
			ft_memmove.c	ft_memset.c		ft_strcat.c		ft_strncat.c	\
			ft_strlcat.c	ft_strchr.c		ft_strcmp.c		ft_strncmp.c	\
			ft_strcpy.c		ft_strncpy.c	ft_strlen.c		ft_strrchr.c	\
			ft_strstr.c		ft_strnstr.c	ft_strdup.c		ft_bzero.c		\
			ft_memalloc.c	ft_memdel.c		ft_strnew.c		ft_strdel.c		\
			ft_strclr.c		ft_striter.c	ft_striteri.c	ft_strmap.c		\
			ft_strmapi.c	ft_strequ.c		ft_strnequ.c	ft_strsub.c		\
			ft_strjoin.c	ft_strtrim.c	ft_strsplit.c	ft_strcchr.c	\
			ft_strxcchr.c	ft_sstrtostr.c	ft_sstrlen.c	ft_sstrnew.c	\
			ft_sstrdel.c	ft_sstradd.c	ft_sstrdup.c	ft_strcchr_rev.c

SRC_NAME +=	ft_isalnum.c	ft_isalpha.c	ft_isdigit.c	ft_islower.c	\
			ft_isprint.c	ft_isupper.c	ft_isascii.c	ft_toupper.c	\
			ft_tolower.c

SRC_NAME +=	ft_atoi.c		ft_itoa.c		ft_itoa_long.c

SRC_NAME +=	ft_putchar.c	ft_putstr.c		ft_putnbr.c		ft_putendl.c	\
			ft_putchar_fd.c	ft_putstr_fd.c	ft_putnbr_fd.c	ft_putendl_fd.c	\
			ft_pputstr.c	ft_pputnbr.c	ft_pputendl.c	ft_putcolor_fd.c

SRC_NAME +=	get_next_line.c	ft_rgb_injector.c	ft_rgb_extractor.c

SRC_NAME +=	ft_strtolower.c	ft_strtoupper.c	ft_strcapital.c	ft_enumword.c	\
			ft_enumletter.c	ft_length_int.c	ft_sqrt.c		ft_pow.c		\
			ft_factor.c		ft_length_long.c

all: $(NAME)

$(NAME): $(OBJ)
	@echo "$(OKC)\nlibft: Creating $(NAME): \c"
	@ar rc $(NAME) $(OBJ)
	@echo "$(OKC)OK. \c"
	@echo "$(OKC)Generating index$(NOC): \c"
	@ranlib $(NAME)
	@echo "$(OKC)OK.\nlibft: compilation success!$(NOC)"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@echo "$(OKC)\c"
	@mkdir -p $(OBJ_PATH)
	@$(CC) $(CC_FLAGS) $(INC) -o $@ -c $<
	@echo -n █
	@echo "$(NOC)\c"

clean:
	@echo "$(WAC)libft: Removing libft ./obj/$(NOC)"
	@rm -rf $(OBJ_PATH)

fclean: clean
	@echo "$(WAC)libft: Removing $(NAME)$(NOC)"
	@rm -f $(NAME)
	@rm -rf $(OBJ_PATH)

re: fclean all
