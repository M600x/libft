# libft
OSX Status: ![Build Status](https://travis-ci.org/m600x/libft.svg?branch=master)

Standard library of various function needed along the cursus of 42.
It's also the first project.

Update as needed or required.

Doesn't contain all the functions to validate the project (i.e. bonus list).
Don't copy if you don't --- at least --- understand what you are copying. It won't help you at the end.

<p align="center">CHAR ARRAY</p>
Function|Return|Description
:--|:-:|:--
ft_strnew|<br><br>void \*|**void \*ft_strnew(size_t size);**<br>Allocate a new char\* of size.<br>Return a new char\* or NULL.|
ft_strclr||**void ft_strclr(char \*s);**<br>Fill the char \*s with EOF data.|
ft_strdel|<br><br>void \*|**void \*ft_strdel(char \*\*as);**<br>Free and put the pointer to NULL of char \*as<br>Return NULL.|
ft_strlen|<br><br>size_t|**size_t ft_strlen(const char \*s);**<br>Calculate the size of the char \*s<br>Return the size of char \*s.|
ft_strdup|<br><br>char \*|**char \*ft_strdup(const char \*s);**<br>Duplicate the char \*s.<br>Return a new char \* or NULL.|
ft_strjoin|<br><br>char \*|**char \*ft_strjoin(char const \*s1, char const \*s2);**<br>Allocate and join char \*s1 and char \*s2 in one unique char \*.<br>Return a new char\* or NULL.|
ft_strsub|<br><br>char \*|**char \*ft_strsub(char const \*s, unsigned int start, size_t len);**<br>Allocate and copy a part of the char \*s starting at start of size len.<br>Return a new char\* or NULL.|
ft_strtrim|<br><br>char \*|**char \*ft_strtrim(char const \*s);**<br>Allocate and return char \*s without the leading and ending whitespaces.<br>Return a new char\* or NULL.|
ft_strcat|<br><br>char \*|**char \*ft_strcat(char \*dest, const char \*src);**<br>Copy char \*dest to char \*src and terminate it with '\0'<br>Return char \*dest.|
ft_strncat|<br><br>char \*|**char \*ft_strncat(char \*dest, const char \*src, size_t n);**<br>Copy char \*dest to char \*src for n length and terminate it with '\0'<br>Return char \*dest.|
ft_strlcat|<br><br>size_t|**size_t ft_strlcat(char \*dst, const char \*src, size_t size);**<br>Copy char \*dest to char \*src and ensure that there's enough space.<br>Return the total size of \*dest.|
ft_strcpy|<br><br>char \*|**char \*ft_strcpy(char \*dest, const char \*src);**<br>Copy char \*src in char \*dest.<br>Return char \*dest|
ft_strncpy|<br><br>char \*|**char \*ft_strncpy(char \*dest, const char \*src, size_t n);**<br>Copy char \*src in char \*dest for n length.<br>Return char \*dest.|

<p align="center">CHAR ARRAY MANIPULATION</p>
Function|Return|Description
:--|:-:|:--
ft_strtolower|<br><br>char \*|**char \*ft_strtolower(char \*str);**<br>Transform each char that is upper to the lower version.<br>Return char \*str.|
ft_strtoupper|<br><br>char \*|**char \*ft_strtoupper(char \*str);**<br>Transform each char that is lower to the upper version.<br>Return char \*str.|
ft_strcapital|<br><br>char \*|**char \*ft_strcapital(char \*str);**<br>Transform each first letter of word (delimited by whitespaces) with a capital letter.<br>Return char \*str.|
ft_striter||**void ft_striter(char \*s, void (\*f)(char \*));**<br>Pass each char of char \*s to the function f.|
ft_striteri||**void ft_striteri(char \*s, void (\*f)(unsigned int, char \*));**<br>Pass each char of char \*s to the function f along with the index.|
ft_strmap|<br><br>char \*|**char \*ft_strmap(char const \*s, char (\*f)(char));**<br>Like ft_striter but allocate a new char \* for the result.<br>Return a new char \* or NULL.|
ft_strmapi|<br><br>char \*|**char \*ft_strmapi(char const \*s, char (\*f)(unsigned int, char));**<br>Like ft_striteri but allocate a new char \* for the result.<br>Return a new char \* or NULL.|

<p align="center">CHAR ARRAY SEARCH</p>
Function|Return|Description
:--|:-:|:--
ft_strchr|<br><br>char \*|**char \*ft_strchr(const char \*s, int c);**<br>Search the int c (as char) in the const char \*s.<br>Return a pointer or NULL.|
ft_strrchr|<br><br>char \*|**char *ft_strrchr(const char \*s, int c);**<br>Search the int c (as char) in const char \*s starting from the end.<br>Return a pointer or NULL.|
ft_strcmp|<br><br>int|**int ft_strcmp(const char \*s1, const char \*s2);**<br>Compare \*s1 and \*s2. <br>Return the differential value of the two char or ZERO.|
ft_strncmp|<br><br>int|**int ft_strncmp(const char \*s1, const char \*s2, size_t n);**<br>Compare \*s1 and \*s2 for n time.<br>Return the differencial value of the two char or ZERO.|
ft_strequ|<br><br>int|**int ft_strequ(const char \*s1, const char \*s2);**<br>Compare const char \*s1 and const char \*s2.<br>Return 1 if strictly equal or ZERO.|
ft_strnequ|<br><br>int|**int ft_strnequ(const char \*s1, const char \*s2, size_t n);**<br>Compare const char \*s1 and const char \*s2 for n length.<br>Return 1 if strictly equal for n length or ZERO.|
ft_strxcchr|<br><br>int|**int ft_strxcchr(const char \*s, int c);**<br>Search the int c (as char) in const char \*s.<br>Return the number of time found or ZERO.|
ft_strcchr|<br><br>int|**int ft_strcchr(const char \*s, int c);**<br>Search the int c (as char) in const char \*s.<br>Return the position if found or ZERO.|
ft_strcchr_rev|<br><br>int|**int ft_strcchr_rev(const char \*s, int c);**<br>Search the int c (as char) in const char \*s starting at the end.<br>Return the position if found or ZERO.|
ft_strstr|<br><br>char*|**char \*ft_strstr(const char \*s1, const char \*s2);**<br>Search the const char \*s2 in const char \*s1.<br>Return the pointer of the start of where it match or NULL.|
ft_strnstr|<br><br>char*|**char \*ft_strnstr(const char \*s1, const char \*s2, size_t n);**<br>Search the const char \*s2 in const char \*s1 for n length<br>Return the pointer of the start of where it match or NULL.|

<p align="center">CHAR ARRAY 2D</p>
Function|Return|Description
:--|:-:|:--
ft_sstrnew|<br><br>char \*\*|**char \*\*ft_sstrnew(size_t size);**<br>Allocate a new char \*\* of size<br>Return a new char \*\* or NULL.|
ft_sstrdel||**void ft_sstrdel(char \*\*s);**<br>Delete the \*\*char|
ft_sstrlen|<br><br>int|**int ft_sstrlen(char \*\*s);**<br>Calculate the size of char \*\*s<br>Return the size or ZERO.|
ft_sstrdup|<br><br>char \*\*|**char \*\*ft_sstrdup(char \*\*src);**<br>Allocate and duplicate the char \*\*src<br>Return a new char \*\* or NULL.|
ft_sstradd|<br><br>char \*\*|**char \*\*ft_sstradd(char \*\*s, char \*add);**<br>Allocate and duplicate the char \*\*src and add char \*add to the end. Free char \*s.<br>Return a new char \*\* or NULL.|

<p align="center">MEMORY</p>
Function|Return|Description
:--|:-:|:--
ft_memchr|<br><br>void \*|**void \*ft_memchr(const void \*s, int c, size_t n);**<br>Search in void \*s (as unsigned char) an occurence of int c (as unsigned char) of n length starting at the end.<br>Return a void \* or NULL.|
ft_memcmp|<br><br>int|**int ft_memcmp(const void \*s1, const void \*s2, size_t n);**<br>Compare const void \*s1 and const void \*s2 for n length.<br>Return the differential value of s1 and s2 or ZERO.|
ft_memcpy|<br><br>void \*|**void \*ft_memcpy(void \*dst, const void \*src, size_t n);**<br>Copy const void \*src to const void \*dst for n length.<br>Return void \*dest.|
ft_memccpy|<br><br><br><br>void \*|**void \*ft_memccpy(void \*dst, const void \*src, int c, size_t n);**<br>The ft_memccpy() function copies bytes from string src to string dst.  If the character c (as converted to an unsigned char) occurs in the string src, the copy stops and a pointer to the byte after the copy of c in the string dst is returned.<br>Return a void \* or NULL.|
ft_memmove|<br><br>void \*|**void \*ft_memmove(void \*dst, const void \*src, size_t len);**<br>Copy const void \*src to void \*dst for length len.<br>Return void \*dst.|
ft_memset|<br><br>void \*|**void \*ft_memset(void \*b, int c, size_t len);**<br>Set the memory pointed by void \*b of length len with int c.<br>Return void \*b.|
ft_bzero||**void ft_bzero(void \*s, size_t n);**<br>Set the memory pointed by void \*s of length n with '\0'.|
ft_memalloc|<br><br>void \*|**void \*ft_memalloc(size_t size);**<br>Allocate memory of size and fill it with '\0'.<br>Return a new void \* or NULL.|
ft_memdel|<br><br>void \*|**void \*ft_memdel(void \*\*ap);**<br>Free the void \*\*ap and set it to NULL.<br>Return NULL.|


<p align="center">DISPLAY</p>
Function|Return|Description
:--|:-:|:--
ft_putchar||**void ft_putchar(char c);**<br>Print the char c.|
ft_putstr||**void ft_putstr(char const \*s);**<br>Print the char \*s.|
ft_putnbr||**void ft_putnbr(int n);**<br>Print the int n.|
ft_putendl||**void ft_putendl(char const \*s);**<br>Print the char \*s with a newline.|
ft_putchar_fd||**void ft_putchar_fd(char c, int fd);**<br>Print char c to the specified fd|
ft_putstr_fd||**void ft_putstr_fd(char const \*s, int fd);**<br>Print the char \*s to the specified fd.|
ft_putnbr_fd||**void ft_putnbr_fd(int n, int fd);**<br>Print the int n to the specified fd.|
ft_putendl_fd||**void ft_putendl_fd(char const \*s, int fd);**<br>Print the char \*s with a new line to the specified fd.|
ft_pputstr||**void ft_pputstr(char const \*s, char const *p);**<br>Print char \*s and char \*p.|
ft_pputnbr||**void ft_pputnbr(int n, char const \*p);**<br>Print the int n and char \*p.|
ft_pputendl||**void ft_pputendl(char const \*s, char const \*p);**<br>Print the char \*s and char \*p with a newline.|
ft_putcolor_fd||**void ft_putcolor_fd(char \*str, char \*color, int fd);**<br>Print the char \*str with the color char \*color on the specified fd|

<p align="center">CHECKING</p>
Function|Return|Description
:--|:-:|:--
ft_isalnum|<br>int|**int ft_isalnum(int c);**<br>Return 1 if the int c (as char) is alphanumerical or 0.|
ft_isalpha|<br>int|**int ft_isalpha(int c);**<br>Return 1 if the int c (as char) is alphabet or 0.|
ft_isdigit|<br>int|**int ft_isdigit(int c);**<br>Return 1 if the int c (as char) is digit or 0.|
ft_islower|<br>int|**int ft_islower(int c);**<br>Return 1 if the int c (as char) is lower letter or 0.|
ft_isprint|<br>int|**int ft_isprint(int c);**<br>Return 1 if the int c (as char) is printable or 0.|
ft_isupper|<br>int|**int ft_isupper(int c);**<br>Return 1 if the int c (as char) is upper letter or 0.|
ft_isascii|<br>int|**int ft_isascii(int c);**<br>Return 1 if the int c (as char) is in the standard ASCII table or 0.|
ft_toupper|<br>int|**int ft_toupper(int c);**<br>Return the value of the int c (as char) to the upper value (-48)|
ft_tolower|<br>int|**int ft_tolower(int c);**<br>Return the value of the int c (as char) to the lower value (+48)|

<p align="center">CONVERSION</p>
Function|Return|Description
:--|:-:|:--
ft_strsplit|<br><br>char \*\*|**char **ft_strsplit(char const \*s, char c);**<br>Split  char \*s with char c in between<br>Return a new char \*\* or NULL.|
ft_sstrtostr|<br><br>char \*|**char \*ft_sstrtostr(char \*\*s, char \*sep);**<br>Allocate a char \* and concatenate the char \*\*s with char \*sep in between<br>Return a new char \* or NULL.|
ft_atoi|<br><br>int|**int ft_atoi(const char \*str);**<br>Convert the const char \*str to an int.<br>Return an int or ZERO.|
ft_itoa|<br><br>char \*|**char \*ft_itoa(int n);**<br>Convert the int n to a char \*<br>Return a new char \* or NULL.|
ft_itoa_long|<br><br>char \*|**char \*ft_itoa_long(long long nb);**<br>Convert the long long nb to a char \*<br>Return a new char \* or NULL.|

<p align="center">MATH</p>
Function|Return|Description
:--|:-:|:--
ft_enumword|<br><br>size_t|**size_t ft_enumword(const char \*s, char c);**<br>Calculate the number of word in const char \*s separate with char c<br>Return the number of "word" in char \*s or ZERO.|
ft_enumletter|<br><br>size_t|**size_t ft_enumletter(const char \*s, char c);**<br>Calculate the number of letter in const char \*s without the char c<br>Return the number of char that is different from char c or ZERO.|
ft_length_int|<br><br>int|**int ft_length_int(int n);**<br>Calculate the number of digit in the int n<br>Return an int or ZERO.|
ft_length_long |<br><br>int|**int ft_length_long(long long n);**<br>Calculate the nimber of digit in the long long n<br>Return an int or ZERO.|
ft_sqrt|<br><br>int|**int ft_sqrt(int nb);**<br>Calculate the square number of int nb.<br>Return an int or ZERO.|
ft_pow|<br><br>int|**int ft_pow(int nb, int power);**<br>Calculate the power number of int nb.<br>Return an int or ZERO.|
ft_factor|<br><br>int|**int ft_factor(int nb);**<br>Calculate the factorial of int nb.<br>Return an int or ZERO.|

<p align="center">OTHER</p>
Function|Return|Description
:--|:-:|:--
get\_next\_line|<br><br>int|**int get_next_line(int fd, char \*\*line);**<br>Read the int fd and store the result in char \*\*line.<br>Return 1 if the fd is not finished.<br>Return -1 if an error occurs.<br>Return 0 if the read is finished.|
