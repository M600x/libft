/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alao <alao@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 16:36:16 by alao              #+#    #+#             */
/*   Updated: 2016/12/02 13:57:55 by alao             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stddef.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>

# define BUFF_SIZE	1
# define MFD		1000

typedef struct		s_rd
{
	char			*now;
	char			*prev;
	long int		len;
	long int		ri;
	long int		rs;
	long int		fs;
}					t_rd;

/*
** STRING.H RELEVANT & 42 specific separated
*/

void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memset(void *b, int c, size_t len);
char				*ft_strcat(char *dest, const char *src);
char				*ft_strncat(char *dest, const char *src, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strchr(const char *s, int c);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strcpy(char *dest, const char *src);
char				*ft_strncpy(char *dest, const char *src, size_t n);
size_t				ft_strlen(const char *s);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
char				*ft_strdup(const char *s);
void				ft_bzero(void *s, size_t n);

void				*ft_memalloc(size_t size);
void				*ft_memdel(void **ap);
void				*ft_strnew(size_t size);
void				*ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int					ft_strequ(const char *s1, const char *s2);
int					ft_strnequ(const char *s1, const char *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);
int					ft_strcchr(const char *s, int c);
int					ft_strxcchr(const char *s, int c);
char				*ft_sstrtostr(char **s, char *sep);
int					ft_sstrlen(char **s);
char				**ft_sstrnew(size_t size);
void				ft_sstrdel(char **s);
char				**ft_sstradd(char **s, char *add);
char				**ft_sstrdup(char **src);
int					ft_strcchr_rev(const char *s, int c);

/*
** CTYPE.H RELEVANT
*/

int					ft_isalnum(int c);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_islower(int c);
int					ft_isprint(int c);
int					ft_isupper(int c);
int					ft_isascii(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);

/*
** STDLIB.H RELEVANT & 42 itoa (char to int)
*/

int					ft_atoi(const char *str);
char				*ft_itoa(int n);
char				*ft_itoa_long(long long nb);

/*
** Display purpose
*/
void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putnbr(int n);
void				ft_putendl(char const *s);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_pputstr(char const *s, char const *p);
void				ft_pputnbr(int n, char const *p);
void				ft_pputendl(char const *s, char const *p);
void				ft_putcolor_fd(char *str, char *color, int fd);

/*
** 42 Specific
*/

int					get_next_line(int fd, char **line);

char				*ft_strtolower(char *str);
char				*ft_strtoupper(char *str);
char				*ft_strcapital(char *str);
size_t				ft_enumword(const char *s, char c);
size_t				ft_enumletter(const char *s, char c);
int					ft_length_int(int n);
int					ft_length_long(long long n);
int					ft_sqrt(int nb);
int					ft_pow(int nb, int power);
int					ft_factor(int nb);
int					ft_rgb_injector(int red, int green, int blue);
int					ft_rgb_extractor(int color, char c);

#endif
